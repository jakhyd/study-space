package com.best.Thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/8
 **/
class MyThread1 implements Runnable {
    @Override
    public void run() {
        System.out.println("Runnable 接口");
    }
}
class MyThread implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        System.out.println(Thread.currentThread().getName() + "**********come in Callable");
        //暂停一会儿线程
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 1024;
    }
}
public class CallableDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //FutrueTask(Callable<V> callable)
        FutureTask<Integer> futureTask = new FutureTask<>(new MyThread());
        new Thread(futureTask, "AA").start();

        System.out.println(Thread.currentThread().getName() + "***********");
        int result01 = 100;

//        while (!futureTask.isDone()){}
        int result02 = futureTask.get();
        System.out.println("*******result: " + (result01 + result02));
    }
}
