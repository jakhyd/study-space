package com.best.CAS;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/5
 **/

import java.util.concurrent.atomic.AtomicInteger;

/**
 * CAS是什么？ ->compare and swap
 * 比较并交换
 */
public class CASDemo {
    public static void main(String[] args) {

        AtomicInteger atomicInteger = new AtomicInteger(5);

        //模拟T1线程
        System.out.println(atomicInteger.compareAndSet(5, 2019) + "\t current data: " + atomicInteger.get());

        //模拟T2线程
        System.out.println(atomicInteger.compareAndSet(5, 1024) + "\t current data: " + atomicInteger.get());

        //i++, 解决了i++在多线程情况下的线程安全问题
        atomicInteger.getAndIncrement();
    }
}
