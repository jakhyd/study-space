package com.best.CAS;

import lombok.*;

import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/6
 **/
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
class User {
    private String name;
    private int age;
}
public class AtomicReferenceDemo {

    public static void main(String[] args) {

        User a = new User("a", 22);
        User b = new User("b", 23);
        AtomicReference<User> atomicReference = new AtomicReference<>();
        atomicReference.set(a);
        System.out.println(atomicReference.compareAndSet(a, b) + "\t" + atomicReference.get().toString());

        System.out.println(atomicReference.compareAndSet(a, b) + "\t" + atomicReference.get().toString());
    }
}
