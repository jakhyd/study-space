package com.best.Lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/7
 **/

class Phone1 implements Runnable {

    private Lock lock = new ReentrantLock();

    @Override
    public void run() {
        get();
    }

    private void get() {
        //可以出现多个lock,unlock，但是，lock，unlock数量要匹配
        lock.lock();
        lock.lock();
        try {
            //线程可以进入任何一个它已经拥有的锁
            //所同步着的代码块
            System.out.println(Thread.currentThread().getName() + "\t invoked get");
            set();
        } finally {
            lock.unlock();
            lock.unlock();
        }
    }

    private void set() {
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + "\t ***** invoked set");
        } finally {
            lock.unlock();
        }
    }
}
public class ReentrantLockDemo {
    public static void main(String[] args) {
        Phone1 phone1 = new Phone1();
        Thread t3 = new Thread(phone1, "t3");
        Thread t4 = new Thread(phone1, "t4");
        t3.start();
        t4.start();
    }
}
