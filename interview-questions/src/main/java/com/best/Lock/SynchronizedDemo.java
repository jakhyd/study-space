package com.best.Lock;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/7
 **/
//资源类
class Phone {
    //同步方法可以进入另外一个同步方法
    //同步方法
    public synchronized void sendSMS() throws Exception {
        System.out.println(Thread.currentThread().getName() + "\t invoked sendSMS()");
        sendEmail();
    }

    //同步方法
    public synchronized void sendEmail() throws Exception {
        System.out.println(Thread.currentThread().getName() + "\t ####invoked sendEmail()");
    }
}
//可重入锁也叫递归锁，作用防止死锁
public class SynchronizedDemo {
    public static void main(String[] args) {
        Phone phone = new Phone();
        //线程操纵资源类
        new Thread(() -> {
            try {
                phone.sendSMS();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, "t1").start();

        new Thread(() -> {
            try {
                phone.sendSMS();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, "t2").start();
    }
}
