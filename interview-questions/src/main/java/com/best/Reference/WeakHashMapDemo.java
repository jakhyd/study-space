package com.best.Reference;

import java.util.HashMap;
import java.util.WeakHashMap;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/11
 **/
public class WeakHashMapDemo {
    public static void main(String[] args) {
        myHashMap();
        System.out.println("*****************************");
        myWeankHashMap();
    }

    @SuppressWarnings("all")
    private static void myWeankHashMap() {
        WeakHashMap<Integer, String> map = new WeakHashMap<>();
        Integer key = new Integer(2);
        String value = "WeakHashMap";
        map.put(key, value);
        System.out.println(map);

        key = null;
        System.out.println(map);

        System.gc();
        System.out.println(map + "\t" + map.size());
    }

    @SuppressWarnings("all")
    private static void myHashMap() {
        HashMap<Integer, String> map = new HashMap<>();
        Integer key = new Integer(1);
        String value = "HashMap";
        map.put(key, value);
        System.out.println(map);

        key = null;
        System.out.println(map);

        System.gc();
        System.out.println(map + "\t" + map.size());
    }
}
