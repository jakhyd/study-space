package com.best.Reference;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/11
 **/
public class StrongReferenceDemo {
    public int[] twoSum(int[] nums, int target) {
        int[] arr = null;
        for (int i=0; i<nums.length; i++) {
            for (int j=0; j<nums.length; j++) {
                if(i!=j) {
                    if ((nums[i] + nums[j]) == target) {
                        arr[0] = i;
                        arr[1] = j;
                    }
                }
            }
        }
        return arr;
    }
}
