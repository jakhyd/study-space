package com.best.Reference;

import java.lang.ref.WeakReference;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/11
 **/
public class WeakReferenceDemo {
    public static void main(String[] args) {
        Object o1 = new Object();
        WeakReference<Object> weakReference = new WeakReference<>(o1);
        System.out.println(o1);
        System.out.println(weakReference.get());

        o1 = null;
        System.gc();
        System.out.println("=====================");

        System.out.println(o1);
        System.out.println(weakReference.get());
    }
}
