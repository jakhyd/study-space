package com.best.JUC;

import com.best.CountryEnum;

import java.util.concurrent.CountDownLatch;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/1
 **/
public class CountDownLatchDemo {

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(6);
        for (int i=1; i <= 6; i++) {
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + "\t 国，被灭");
                countDownLatch.countDown();
            }, CountryEnum.forEach_CountryEnmu(i).getRetMessage()).start();
        }
        countDownLatch.await();
        System.out.println(Thread.currentThread().getName() + "\t *******************秦帝国，一统华夏");
    }


    public static void closeDoor(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(6); //设置班级同学总数为6
        for (int i=0; i < 6; i++) {
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + "\t上完自习，离开教室");
                countDownLatch.countDown(); //班级同学走一个减一个1
            }, String.valueOf(i)).start();
        }
        countDownLatch.await(); //上面的走完，我下面的主线程才能走，控制前面的任务完成以后，才能进行后面的任务
        System.out.println(Thread.currentThread().getName() + "\t **************班长最后关门走人");
    }
}
