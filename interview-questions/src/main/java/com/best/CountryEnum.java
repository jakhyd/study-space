package com.best;

import lombok.Getter;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/1
 **/
public enum CountryEnum {

    ONE(1, "齐"),
    TWO(2, "楚"),
    THREE(3, "燕"),
    FOUR(4, "赵"),
    FIVE(5, "魏"),
    SIX(6, "韩");

    @Getter
    private Integer retCode;

    @Getter
    private String retMessage;

    CountryEnum(Integer retCode, String retMessage) {
        this.retCode = retCode;
        this.retMessage = retMessage;
    }
    public static CountryEnum forEach_CountryEnmu(int index) {
        CountryEnum[] enums = CountryEnum.values();
        for (CountryEnum countryEnum : enums) {
            if (index == countryEnum.getRetCode()) {
                return countryEnum;
            }
        }
        return null;
    }
}
