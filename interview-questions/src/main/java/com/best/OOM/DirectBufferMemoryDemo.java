package com.best.OOM;

import java.nio.ByteBuffer;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/12
 **/
public class DirectBufferMemoryDemo {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("配置的maxDirectMemory: " + (sun.misc.VM.maxDirectMemory()/(double)1024/1024) + "MB");
        Thread.sleep(3000);
        //-XX:MaxDirectMemorySize=5M 我们配置为5MB，但实际使用6MB，故意使坏
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(6 * 1024 * 1024);
    }
}
