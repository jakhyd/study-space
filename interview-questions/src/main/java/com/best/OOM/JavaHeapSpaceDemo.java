package com.best.OOM;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/11
 **/
public class JavaHeapSpaceDemo {
    public static void main(String[] args) {

        //方式一
        byte[] bytes = new byte[80 * 1024 * 1024];

//        //方式二
//        String str = "test";
//
//        while (true) {
//            str += str + new Random().nextInt(10000);
//        }
    }
}
