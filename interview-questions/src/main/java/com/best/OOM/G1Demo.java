package com.best.OOM;

import java.util.Random;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/14
 **/
// -Xms10m -Xmx10m -XX:+PrintGCDetails -XX:+PrintCommandLineFlags -XX:+UseG1GC
public class G1Demo {
    public static void main(String[] args) {
        String str = "test";
        while (true) {
            str += str + new Random().nextInt(111);
            str.intern();
        }
    }
}
