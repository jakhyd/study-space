package com.best.JVM;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/8
 **/
public class MyObject {
    public static void main(String[] args) {
        Object object = new Object();
        System.out.println(object.getClass().getClassLoader());
//        System.out.println(object.getClass().getClassLoader().getParent());//java.lang.NullPointerException

        System.out.println("***************************");
        MyObject myObject = new MyObject();
        System.out.println(myObject.getClass().getClassLoader());
        System.out.println(myObject.getClass().getClassLoader().getParent());
        System.out.println(myObject.getClass().getClassLoader().getParent().getParent());
    }
}
