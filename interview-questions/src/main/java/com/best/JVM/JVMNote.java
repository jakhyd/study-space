package com.best.JVM;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/9
 **/
class Person {

}
//栈保存哪些东西
//8种基本类型的变量+对象的引用变量+实例方法都是在函数的栈内存中分配
public class JVMNote {

    //输入参数和输出参数以及方法内的变量
    public int add(int x, int y) {
        int result = -1;
        result = x + y;
        return result;
    }

    public static void m1() {
        System.out.println("222");
        System.out.println("****m1");
        System.out.println("333");
        m1();
    }
    public static void main(String[] args) {
        System.out.println("111");
        Person p1 = new Person();
        m1();
        System.out.println("444");

    }
}
