package com.best.JVM;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/9
 **/
class Car {}
public class Stack {

    public static void main(String[] args) {

        List<byte[]> list = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            list.add(new byte[1024 * 1024]);
        }
    }
    public static void m1() {

        Car car1 = new Car();

        Car car2 = new Car();

        Car car3 = new Car();
    }


}
