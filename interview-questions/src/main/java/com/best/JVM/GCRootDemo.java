package com.best.JVM;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/10
 **/
/*
1.虚拟机栈(栈帧中的本地变量表)中引用的对象；
2.方法区中的类静态属性引用的对象
3.方法区中常量引用的对象
4.本地方法栈中JNI(即一般说的Native方法)中引用的对象
 */
public class GCRootDemo {

//    private static GCRootDemo2 t2;

//    private static final GCRootDemo3 t3 = new GCRootDemo3();

    public static void m1() {
        GCRootDemo t1 = new GCRootDemo();
    }
}
