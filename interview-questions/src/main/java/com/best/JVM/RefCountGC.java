package com.best.JVM;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/9
 **/
public class RefCountGC  {
    //这个成员属性唯一的作用就是占用一点内存
    private byte[] bigSize = new byte[2 * 1024 * 1024];
    Object instance = null;

    public static void main(String[] args) {
        RefCountGC objectA = new RefCountGC();
        RefCountGC objectB = new RefCountGC();
        objectA.instance = objectB;
        objectB.instance = objectA;
        objectA = null;
        objectB = null;
        //注意System.gc执行完之后并不会立即GC
        System.gc();
    }
}
