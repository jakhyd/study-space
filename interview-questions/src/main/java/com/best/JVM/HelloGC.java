package com.best.JVM;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/11
 **/
public class HelloGC {
    public static void main(String[] args) throws InterruptedException {
//        byte[] bytes = new byte[50 * 1024 * 1024];
        System.out.println("Hello GC");
        Thread.sleep(Integer.MAX_VALUE);
    }
}
