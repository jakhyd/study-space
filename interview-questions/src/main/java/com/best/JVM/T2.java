package com.best.JVM;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/12/9
 **/
public class T2 {
    public static void main(String[] args) {
        long maxMemory = Runtime.getRuntime().maxMemory();
        long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("-Xmx:MAX_MEMORY = " + maxMemory + "(字节、)" + (maxMemory / (double) 1024 / 1024) + "MB");
        System.out.println("-Xms:TOTOL_MEMORY = " + totalMemory + "(字节、)" + (totalMemory / (double) 1024 / 1024) + "MB");
    }
}
