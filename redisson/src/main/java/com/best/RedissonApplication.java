package com.best;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/29
 **/

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class RedissonApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedissonApplication.class, args);
    }
}
