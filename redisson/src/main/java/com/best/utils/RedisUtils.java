package com.best.utils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author Jiang Akang
 * employeeId: BG435424
 * @date 2020/11/30
 **/
public class RedisUtils {
    private static JedisPool jedisPool;

    static {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(20);
        jedisPoolConfig.setMaxIdle(10);
        new JedisPool(jedisPoolConfig, "192.168.208.129", 6379);
    }

    public static Jedis getJedis() throws Exception {
        if (jedisPool != null) {
            return jedisPool.getResource();
        } else {
            throw new Exception("JedisPool is not ok");
        }
    }
}
